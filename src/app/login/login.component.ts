import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  public user: string = ""
  public pass: string = ""

  constructor(private toastController: ToastController, public navctrl: NavController) { }

  ngOnInit() {}

  login() {
    if (this.pass == "titlan") {
      switch (this.user) {
        case "cajero":
          localStorage.setItem("userType", "01")
          this.navctrl.navigateForward("home/Dashboard")
          break;
        case "mesero":
          localStorage.setItem("userType", "02")
          this.navctrl.navigateForward("home/Carta")
          break;
        case "almacen":
          localStorage.setItem("userType", "03")
          this.navctrl.navigateForward("home/Dashboard")
          break;
        default:
          this.presentToast("Usuario y/o contraseña incorrectos");
          break;
      }
    }
    else {
      this.presentToast("Usuario y/o contraseña incorrectos");
    }
  }

  async presentToast( msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2500
    });
    toast.present();
  }

}
